#include "ofMain.h"

#include "ana_build.h"
#include "ana_features.h"
#include "color/ColorQuantizationTest.h"
#include "calc/ScalarRPN.h"
#include "calc/ScalarLisp.h"
#include "calc/ScalarLispTest.h"
#include "lsys/LsysMachineTest.h"
#include "ofApp.h"

int main(int argc, char *argv[]) {
#ifdef WITH_testcases
    ColorQuantizationTest::runTestCases();
    ScalarRPN::runTestCases();
    ScalarLispTest::runTestCases();
    ScalarLispTest::runProfile();
    LsysMachineTest::runTestCases();
#endif // WITH_testcases

    ofGLWindowSettings settings;
    settings.setGLVersion(3, 3);
    settings.setSize(1000, 1000);
    settings.windowMode = OF_WINDOW;
    ofCreateWindow(settings);
    ofSetWindowTitle("aNa test cases");

    std::shared_ptr<ofApp> app = make_shared<ofApp>(settings);
    ofRunApp(app);
}
