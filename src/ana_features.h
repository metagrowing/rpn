#pragma once

// In a production system test cases can be disabled.
#define WITH_testcases
//#undef WITH_testcases

// If you can not compile some subsystem of 'analog Not analog' on your
// operating system, you can disable features here.
// And recompile all.

// memory usage reads from /proc/self/status
// this is supported on linux systems
#define WITH_memoryusage
//#undef WITH_memoryusage

// reads current the ipv4 address from hostname -I
// this is supported on linux systems
#define WITH_deteced_ipv4
//#undef WITH_deteced_ipv4

// to disable OpenCL remove ofxMSAOpenCL from addons.make too
#define WITH_openCL
//#undef WITH_openCL

// reads joystick events from /dev/input/jsX
#define WITH_js_dev_input
//#undef WITH_WITH_js_dev_input
