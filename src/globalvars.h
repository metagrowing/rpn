#pragma once
extern unsigned int frame;
extern float beat_pos;

extern float smoothedVol;
extern float onset;
extern float minimumThreshold;
extern float decayRate;

extern float u0;
extern float u1;
extern float u2;
extern float u3;
extern float u4;
extern float u5;
extern float u6;
extern float u7;

extern float lx;
extern float ly;
extern float rx;
extern float ry;
extern float l2;
extern float r2;
