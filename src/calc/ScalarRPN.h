#pragma once
#include <random>

#include "ana_features.h"
#include "calc/Noise.h"

#define oprStop        0
#define oprReturn      1 // reserved
#define oprPushConst   2
#define oprPushX       3
#define oprPushY       4
#define oprPushZ       5
#define oprPushW       6
#define oprPushI       7
#define oprPushJ       8
#define oprPushK       9
#define oprPushF      10
#define oprPushLx     11
#define oprPushLy     12
#define oprPushRx     13
#define oprPushRy     14
#define oprPushL2     15
#define oprPushR2     16
#define oprPushOnset  17
#define oprPushVol    18
#define oprPushU0     19
#define oprPushU1     20
#define oprPushU2     21
#define oprPushU3     22
#define oprPushU4     23
#define oprPushU5     24
#define oprPushU6     25
#define oprPushU7     26
#define oprPushMx     27
#define oprPushMy     28

#define oprIfPositive 29
#define oprIfZero     30
#define oprLabel      31
#define oprJmp        32

#define oprAdd         (oprJmp +  1)
#define oprAdd2        (oprJmp +  2) // reserved
#define oprMult        (oprJmp +  3)
#define oprMult2       (oprJmp +  4) // reserved
#define oprSub         (oprJmp +  5)
#define oprSub1        (oprJmp +  6)
#define oprSub2        (oprJmp +  7) // reserved
#define oprDiv         (oprJmp +  8)
#define oprDiv1        (oprJmp +  9)
#define oprDiv2        (oprJmp + 10) // reserved
#define oprUNoise      (oprJmp + 11)
#define oprURandom     (oprJmp + 12)
#define oprSNoise      (oprJmp + 13)
#define oprTurbulence  (oprJmp + 14)
#define oprPow         (oprJmp + 15)
#define oprSqrt        (oprJmp + 16)
#define oprTan         (oprJmp + 17)
#define oprSin         (oprJmp + 18)
#define oprCos         (oprJmp + 19)
#define oprAtan2       (oprJmp + 20)
#define oprLog         (oprJmp + 21)
#define oprMix         (oprJmp + 22)
#define oprStep        (oprJmp + 23)
#define oprPuls        (oprJmp + 24)
#define oprClamp       (oprJmp + 25)
#define oprMax         (oprJmp + 26)
#define oprMin         (oprJmp + 27)
#define oprAbs         (oprJmp + 28)
#define oprSmoothstep  (oprJmp + 29)
#define oprBoxstep     (oprJmp + 30)
#define oprMod         (oprJmp + 31)
#define oprFloor       (oprJmp + 32)
#define oprCeil        (oprJmp + 33)
#define oprGamma       (oprJmp + 34)
#define oprGain        (oprJmp + 35)
#define oprBias        (oprJmp + 36)
#define oprEuclideanDistance (oprJmp + 37)
#define oprAbsoluteDistance  (oprJmp + 38)
#define oprUSin        (oprJmp + 39)
#define oprUCos        (oprJmp + 40)
#define oprASin        (oprJmp + 41)
#define oprACos        (oprJmp + 42)
#define oprTrunc       (oprJmp + 43)
#define oprRound       (oprJmp + 44)
#define oprSign        (oprJmp + 45)
#define oprLog2        (oprJmp + 46)
#define oprSqr         (oprJmp + 47)

#define rpn_epsilon 1.0E-7

class ScalarRPN {
public:
    ScalarRPN();
    virtual ~ScalarRPN();
    void seed(long seed);
    double execute4d3i(double x, double y, double z, double w, int i, int j, int k);
    inline double execute3d1i(double x, double y, double z, int i) {
        return isConst ? const_result : execute4d3i(x, y, z, 0.0, i, 0, 0);
    }
    inline double execute0() {
        return isConst ? const_result : execute4d3i(0.0, 0.0, 0.0, 0.0, 0, 0, 0);
    }

    void clear();
    void appendOpr(int o, double d = 0.0, int i = 0);

    void dumpOpr(int ip, std::string &a);
    std::string dump();

    bool isConst = false;
    double const_result = 0.0;

#ifdef DEBUG
		void protectRPN(bool cond, const char* const msg);
#endif

    ImprovedNoise improved_noise;
    std::mt19937 mt;
    std::uniform_real_distribution<double> dist;
    using dist_t = std::uniform_real_distribution <>;
    using param_t = dist_t::param_type;
    param_t range { 0.0, 1.0 };

    static const int maxTapeLen = 1024;
    int tapeLength = 0;
    int codeTape[maxTapeLen + 1];
    bool gotos = false;
    void *gotoTape[maxTapeLen + 1];
    int iOper[maxTapeLen + 1];
    double dOper[maxTapeLen + 1];
    double dStack[1024 + 1];

public:
#ifdef WITH_testcases
    static void runTestCases();
    void test1();
#endif
};
