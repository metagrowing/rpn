#pragma once

#include "ana_features.h"

#include "ofMain.h"
#include "lsys/LsysMachine.h"
#include "lsys/LsysIntepretation.h"
#include "lsys/LsysTurtle.h"
#include "lsys/LsysCurve.h"

class ofApp: public ofBaseApp {
public:
    ofApp(ofGLWindowSettings settings);

    void setup();
    void update();
    void draw();
    void keyPressed(int key);
private:
    ofGLWindowSettings window_settings;
    ofTrueTypeFont infofont;

    LsysIntepretation* t = nullptr;
    LsysTurtle it;
    LsysCurve  ic;
    LsysMachine* l = nullptr;
    LsysMachine l1;
    LsysMachine l2;
    LsysMachine l3;
    LsysMachine l4;
    LsysMachine l5;
    LsysMachine l6;
    LsysMachine l7;
    LsysMachine l8;
    LsysMachine l9;
    LsysMachine l0;

    void setL1();
    void setL2();
    void setL3();
    void setL4();
    void setL5();
    void setL6();
    void setL7();
    void setL8();
    void setL9();
    void setL0();
    void setLm(LsysMachine& lm,
                const std::string& ignore,
                const std::string &axiom,
                const std::vector<std::string> &predecessor,
                const std::vector<std::string> &successor,
                const std::vector<std::string> &leftContext,
                const std::vector<std::string> &rightContext,
                const std::string depth,
                const std::string forward,
                const std::string angle);
};
