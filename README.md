# This is a incubation environment for analog Not analog

RPN contains:
- A byte code interpreter for for floating point calculations.
- A "compiler" for very simple Lisp like expressions to byte code.
- Functions for expanding Lindenmayer systems.
 - DOL deterministic without context.
 - OL nondeterministic without context.
 - 2L nondeterministic with context of two characters. One character to the left and one character to the right.
 - IL nondeterministic in a context of one character. Either to the left or the right.
- Perlin noise generator

Experimental code. Please do not use in production projects.


[[_TOC_]]

# System requirements
If you plan to compile RPN on your own computer there is no need to have exactly the same environment used for aNa. RPN has lesser dependencies.

## List of hard and software used during development of RPN.
#### Software
- Operating System: 64-Bit Debian GNU/Linux 10
- openFrameworks: of_v0.11.0_linux64gcc6_release
- Compiler: gcc version 8.3.0
- NVIDIA Driver Version: 418.152.00

#### Hardware
- Processors: 16 × Intel® Core™ i9-9900K CPU @ 3.60GHz
- Memory: 32 GiB of RAM
- 3D Accelerator: GeForce RTX 2080 Ti


# Build and start RPN
## Build from source
openFrameworks is used for creative coding. They publish source code, not precompiled binaries.

- Download openFrameworks for linux gcc6 or later. `of_v0.11.0_linux64gcc6_release.tar.gz`
https://openframeworks.cc/download/

- Follow the [Linux install](https://openframeworks.cc/setup/linux-install/) tutorial to build openFrameworks core functions.

-  Set `OF_ROOT` to the folder of your openFrameworks installation

- Optionally test some of openFrameworks OpenGL shader based examples. see: [06_multiTexture](https://github.com/openframeworks/openFrameworks/tree/patch-release/examples/shader/06_multiTexture)

To compile use.
```bash
./clean_build_release
```

### Enabling / disabling features
[src/ana_features.h](./src/ana_features.h) is a header file containing defines to enable or disable some features of RPN and aNa. Edit this file to enable the test cases.
```c++
// In a production system test cases can be disabled.
#define WITH_testcases
//#undef WITH_testcases
```

## Start with GUI
```bash
./run_rpn
```

In the openFrameworks window press keys `0` to `9` to switch between different examples.

To change the color palette press keys `q`, `w` or `e`.

To change the graphical interpretation press keys `a` or `s`.
- `a` turtle turtle grpahic with staight lines.
- `s` Hermite curves. Can produce smooth connection lines. But only for sequences of more than 2 `F`s with `+` or `-` intermixed. Works on branches but not on leaves.

# Expressions and grammar
## Floating point expressions
### Read values
Input values that can be read in expressions.

| read from | description |
| --- | --- |
| x | x coordinate of the current turtle position |
| y | y coordinate of the current turtle position |
| i | current branch depth |
| f | current frame number of the animation |

### Simple arithmetic
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| + | 2 .. n | sum of all parameters | (+ 0.1 x)<br><br>(+ 1 2 3) |
| - | 2 .. n | subtract from first parameter all following parameters | (- 0.1 x)<br><br>(- 1 2 3) |
| * | 2 .. n | multiply all parameters | (* 0.1 x)<br><br>(* 0.05 f x) |
| / | 2 .. n | dived the first parameter by all following parameters | (/ x 2)<br><br>(+ 16 4 2) |
| % | 2 | modulo | (% i 4) |

### Conditionals
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| zero? | 3 | If the first term evaluates to 0 the second term is evaluated. If not the third term is evaluated. | (zero?<br>(/ x 1000)<br>1<br>(+ 1 x)) |
| pos? | 3 | If the first term evaluates to an positive number the second term is evaluated. If not the third term is evaluated. | (pos?<br>x<br>x<br>(* -1.0 x)) |

### Common functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| abs | 1 | absolute value | (abs x) |
| floor | 1 | nearest integer less than x | (floor x) |
| ceil | 1 | nearest integer greater than x | (ceil x) |
| clamp | 3 | returns (min (max x minVal) maxVal) | (clamp x 0.4 0.6) |

### Exponential functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| pow | 2 | returns x raised to the y power | (pow x y) |
| log | 1 | returns the natural logarithm of x | (log (abs x)) |
| sqrt | 1 | square root | (sqrt x) |

### Trigonometry functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| cos | 1 | cosine | (cos 0) |
| ucos | 1 | like cosine but returns values in the range 0.0 to 1.0 | (cos (* 0.1 f)) |
| sin | 1 | sine | (sin 1.57) |
| usin | 1 | like sine but returns values in the range 0.0 to 1.0 | (usin -0.5) |
| tan | 1 | tangent | (tan 0.5) |
| atan | 2 | arc tanget for y over x | (atan y x) |

### Merge values
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| max | 2 | Maximum value | (max 0 x) |
| min | 2 | Minimum value | (min f 100) |
| mix | 3 | linear blend of x and y | (mix x y 0.75) |

### Noise functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| snoise | 3 | simplex noise function | (snoise x y z) |
| unoise | 3 | like noise but returns values approximately in the range 0.0 to 1.0 | ((unoise x y z)) |

see: http://mrl.nyu.edu/~perlin/noise/

### Geometric functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| distance | 2 | euclidean distance between two points  | (TODO) |

## L-System
A L-system grammar for 2D planes.

| character | description |
| --- | --- |
| `F` | move one step forward and draw a line |
| `f` | move one step forward |
| `[` | start a branch<br>branches can be nested |
| `]` | end of branch |
| `+` | turn direction ccw |
| `-` | turn direction cw |
| `\|` | turn direction by 180 degrees|
| `{` | start a filled shape<br>shapes can NOT be nested |
| `}` | end of a filled shape |
| `.` | set one control point for the shape |
| `'` | reserved for color pallet |
| `,` | reserved for color pallet |
| `:` | not implemented yet |
| `;` | not implemented yet |


## To parameterize L-Systems with Lisp like expressions
The characters `F`, `f`, `+`, `-` can parameterized.
This notation can only be used in the successor part of an expansion rule.

Better explained by some examples.

| character<br>followed by expression | description |
| --- | --- |
| `F(0.5)` | do not use the default step size.<br>Instead set the step size of this `F` character to 0.5 |
| `+(snoise x y (* 0.01 f))` | do not use the default branching angle.<br>Instead calculate the branching angle dependent on the current location and the current frame number of the animation |

> :warning: Character used in the Lindenmayer grammar are used in the expressions too. They are interpreted by context.
- `+` turns the direction of the turtle.
- `(+ ` is the addition operator

## C++ samples
### Koch curve as deterministic L-System

![](./stuff/doc/l1.png)

```c++
void ofApp::setL1() {
    // "The Algorithmic Beauty of Plants" Przemyslaw Prusinkiewicz and Aristid Lindenmayer
    // page 10, figure 1.9 f
    // node rewriting
    std::string ignore = "+-";
    std::string axiom = "F-F-F-F";
    std::vector<std::string> predecessor = { "F" };
    std::vector<std::string> successor =   { "F-F+F+FF-F-F+F" };
    std::vector<std::string> leftContext;
    std::vector<std::string> rightContext;
    setLm(l1, ignore, axiom, predecessor, successor, leftContext, rightContext,
                           "4", // depth
                           "1", // default forward step size
                           "(* 0.5 3.14159)"); // default angel
    l = &l1;
}
```

### Animated random walk as non deterministic L-System
Two different rules for `F` are chosen by random.

![](./stuff/doc/l0.png)

(still image only)

```c++
void ofApp::setL0() {
    std::string ignore = "+-";
    std::string axiom = "F";
    std::vector<std::string> predecessor = { "F",                     "F" };
    std::vector<std::string> successor =   { "F+F(sin (* 0.0211 f))", "F-F(sin (* 0.0131 f))" };
    std::vector<std::string> leftContext;
    std::vector<std::string> rightContext;
    setLm(l0, ignore, axiom, predecessor, successor, leftContext, rightContext,
                           "12", // depth
                           "1", // default forward step size
                           "(+ 0.4 (* 0.1 (sin (* 0.0031 f))))"); // default angel
    l = &l0;
}
```

### Parameterized deterministic L-System.
One branching angle is parameterized.

![](./stuff/doc/l8.png)

(still image only)

```c++
void ofApp::setL8() {
    std::string ignore = "+-";
    std::string axiom = "F";
    std::vector<std::string> predecessor = { "F" };
    std::vector<std::string> successor =   { "F[-F][+(+ 0.4 (* 0.2 (sin (* 0.05 f))))F]" };
    std::vector<std::string> leftContext;
    std::vector<std::string> rightContext;
    setLm(l8, ignore, axiom, predecessor, successor, leftContext, rightContext,
                           "9", // depth
                           "(- 10 i)", // default forward step size
                           "0.4"); // default angel
    l = &l8;
}
```

### Animated context dependent deterministic L-System

![](./stuff/doc/l5.png)

(still image only)

```c++
void ofApp::setL5() {
    // "The Algorithmic Beauty of Plants" Przemyslaw Prusinkiewicz and Aristid Lindenmayer
    // page 35, figure 1.31 b
    // Hogeweg and Hesper
    std::string ignore = "+-F";
    std::string axiom = "F1F1F1F1";
    std::vector<std::string> leftContext  = { "0", "0",        "0", "0", "1", "1",   "1", "1", "",  "",  };
    std::vector<std::string> predecessor  = { "0", "0",        "1", "1", "0", "0",   "1", "1", "+", "-", };
    std::vector<std::string> rightContext = { "0", "1",        "0", "1", "0", "1",   "0", "1", "",  "",  };
    std::vector<std::string> successor    = { "1", "1[-F1F1]", "1", "1", "0", "1F1", "1", "0", "-", "+", };
    setLm(l5, ignore, axiom, predecessor, successor, leftContext, rightContext,
                           "30", // depth
                           "(+ 1.2 (snoise x y (* 0.001 f)))", // default forward step size
                           "(+ 0.4 (* 0.2 (snoise y x (* 0.005 f))))"); // default angel
    l = &l5;
}
```


## Copyright
### RPN project source code has GPL 3.0 Copyleft
Copyright © 2020 2021 Thomas Jourdan
This source code is licensed under the [GNU General Public License v3.0](./LICENSE).

### Images and animations
All images and movies in this repository are licensed as creative commons under the following terms: [CC BY-NC 3.0](https://creativecommons.org/licenses/by-nc/3.0/)

## Credits

### The Algorithmic Beauty of Plants
["The Algorithmic Beauty of Plants"](http://algorithmicbotany.org/papers/abop/abop.pdf) A book by Przemyslaw Prusinkiewicz and Aristid Lindenmayer


### RPN is running on top of openFrameworks
openFrameworks is distributed under the [MIT License](https://en.wikipedia.org/wiki/MIT_License). This gives everyone the freedoms to use openFrameworks in any context: commercial or non-commercial, public or private, open or closed source.

### analog Not analog
[aNa](https://gitlab.com/metagrowing/ana) is a live coding system for visuals.
